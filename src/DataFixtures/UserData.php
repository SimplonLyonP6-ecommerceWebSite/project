<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\User;

class UserData extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create("fr_FR");

        for($i = 1; $i <= 20; $i++) {
            $user = new User();
            $user->setUsername($faker->userName());
            $user->setPassword($faker->password());
            $user->setEmail($faker->email());
            $user->setRole(false);
            $user->setCity($faker->city());
            $user->setStreet($faker->streetName());
            $user->setNumber($faker->numberBetween($min = 1, $max = 300));
            $user->setState($faker->country());
            $user->setZipcode(1);

            $manager->persist($user);
        }

        $manager->flush();
    }
}
