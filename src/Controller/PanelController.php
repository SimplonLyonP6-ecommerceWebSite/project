<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\UserRepository;
use App\Entity\User;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Form\UserNoPSWDType;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use App\Repository\OrderHistoryRepository;

class PanelController extends Controller
{
    /**
     * @Route("/user/panel/{id}/edit", name="edit_user")
     * @Route("/admin/edit/{id}", name="edit_a_user")
     */
    public function user(UserRepository $repo, UserInterface $activeUser, Request $req, User $user = null, int $id, OrderHistoryRepository $orders)
    {
        $user = $repo->find($id);
        
        if ($activeUser->getRoles() == true) {
            $form = $this->createForm(UserNoPSWDType::class, $user);
        } 
        elseif ($activeUser===$user) {
        $form = $this->createForm(UserNoPSWDType::class, $activeUser);

        }
        
        $orders = $orders->findAll();
        if($orders) {
            $order = [];
            foreach($orders as $line) {
                if($line->getShoppingCart()->getUser()->getId() == $user->getId()) {
                $order[] = $line;
                }
            }
            if(!$order) {
                $order = null;
            }
        }
        elseif(!$orders) {
            $order = null;
        }
        
        $form->handleRequest($req);
        if ($form->isSubmitted() && $form->isValid()) {

            dump($user);
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
        }
        
        return $this->render('panel/user.html.twig', [
            'controller_name' => 'PanelController',
            'activeUser'=>$activeUser,
            'form'=>$form->createView(),
            "orders" => $order
        ]);
    }


    /**
     * @Route("/admin/panel", name="adminPanel")
     * 
     */
    public function admin(UserRepository $repo, CategoryRepository $repoCat, ProductRepository $repoProd, Request $request)
    {

        $users = $repo->findAll();

        if ($request->isMethod('POST')) {
            $users = $repo->findAllByName($request->get("username"));
        }
        
        $catList = $repoCat->findAll();
        $productList=$repoProd->findAll();

        return $this->render('panel/admin.html.twig', [
            'controller_name' => 'PanelController',
            'users'=>$users,
            'listProd'=>$productList,
            'listCat'=>$catList
        ]);
    }

     /**
     * 
     * @Route("/admin/otherPower/{id}", name="chgRole")
     * 
     */
    public function promote(UserRepository $repo, CategoryRepository $repoCat, ProductRepository $repoProd, int $id = 0)
    {

        $userSpecific = $repo->find($id);
        $activeRole = $userSpecific->getRole();

        if ($activeRole) {
            # code...
            $userSpecific->setRole(0);
        } else {
            # code...
            $userSpecific->setRole(1);
        }
        
            
            $em = $this->getDoctrine()->getManager();

            $em->persist($userSpecific);
            $em->flush();
      

       return $this->redirectToRoute('adminPanel');

      
    }

    //  /**
    //  * 
    //  * @Route("/admin/dismiss/{id}", name="dismissUser")
    //  * 
    //  */
    // public function dismiss(UserRepository $repo, int $id = 0)
    // {

    //     $users = $repo->findAll();
        
    //     $catList = $repoCat->findAll();
    //     $productList=$repoProd->findAll();


    //     $userSpecific = $repo->find($id);
    //     $activeRole = $userSpecific->getRole();

        
    //         $userSpecific->setRole(0);
    //         $em = $this->getDoctrine()->getManager();

    //         $em->persist($userSpecific);
    //         $em->flush();       
        
    //     $this->redirectToRoute('adminPanel');
    // }
}
