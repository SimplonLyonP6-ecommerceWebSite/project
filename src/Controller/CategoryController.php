<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Category;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;

class CategoryController extends Controller
{
    /**
     * @Route("/admin/add-category", name="add_category")
     * @Route("/admin/{id}/edit-category", name="edit_category")
     */
    public function index(Category $category = null, Request $req, ObjectManager $manager)
    {
        if(!$category) {
        $category = new Category();
        }

        $form = $this->createForm(CategoryType::class, $category);

        $form->handleRequest($req);

        if($form->isSubmitted() && $form->isValid()) {

            $manager->persist($category);
            $manager->flush();

            return $this->redirectToRoute("select_edit_category", []);
        }

        return $this->render('category/index.html.twig', [
            'controller_name' => 'CategoryController',
            'categoryForm' => $form->createView(),
            'editMode' => $category->getId() ==! null,
        ]);
    }

    /**
    *  @Route("/admin/edit-category", name="select_edit_category")
    */
    public function selectCategory(CategoryRepository $repo, Request $request) {

        $categories = $repo->findAll();

        if ($request->isMethod('POST')) {

            $id = $request->get("category");
    
            return $this->redirectToRoute("edit_category", ["id" => $id]);
        }

        return $this->render("category/selectCategory.html.twig", ["categories" => $categories, "sup" => false]);
    }

    /**
    *  @Route("/admin/remove-category", name="select_remove_category")
    */
    public function selectRemoveCategory(CategoryRepository $repo, Request $request) {

        $categories = $repo->findAll();

        if ($request->isMethod('POST')) {

            $id = $request->get("category");
    
            return $this->redirectToRoute("remove_category", ["id" => $id]);
        }

        return $this->render("category/selectCategory.html.twig", ["categories" => $categories, "sup" => true]);
    }

    /**
    *  @Route("/admin/{id}/category-remove", name="remove_category")
    */
    public function removeCategory(Category $category) {

        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($category);
        $em->flush();

        return $this->redirectToRoute("select_remove_category", []);
    }
}
